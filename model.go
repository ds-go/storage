package storage

import (
	"database/sql"
)

type Model struct {
	DB    *DBC
	Cache *RedisCache
	Open  bool
	Conn  *sql.DB
}

func NewModel(db *DBC, cache *RedisCache) *Model {
	return &Model{
		DB:    db,
		Cache: cache,
	}
}

func (m *Model) LeaveOpen() error {
	m.Open = true
	db, err := m.DB.Open()
	if err != nil {
		return err
	}
	m.Conn = db
	return nil
}
func (m *Model) Close() {
	if m.Open {
		m.Conn.Close()
	}
}
