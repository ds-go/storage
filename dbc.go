package storage

import (
	"database/sql"
	"strings"

	"github.com/go-sql-driver/mysql"
)

type DBC struct {
	SQLConfig *mysql.Config
	Config    string
	Type      string
}

//NewDatabase returns database config.
func NewDatabase(dbtype, config string) *DBC {
	return &DBC{
		SQLConfig: &mysql.Config{},
		Config:    config,
		Type:      dbtype,
	}
}

func NewMySQLDatabase(config *mysql.Config) *DBC {
	return &DBC{
		SQLConfig: config,
		Config:    config.FormatDSN(),
		Type:      "mysql",
	}
}

//exampleConfig
var exampleConfig = &mysql.Config{
	User:   "user",
	Passwd: "pass",
	DBName: "db",
	Net:    "tcp",
	//Addr:   "localhost:3306",
	//Params           map[string]string // Connection parameters
	//Collation        string            // Connection collation
	//Timeout      time.Duration // Dial timeout
	//ReadTimeout  time.Duration // I/O read timeout
	//WriteTimeout time.Duration // I/O write timeout
}

//Open opens a database connection
//test database connection on open.]
func (dbc *DBC) Open() (db *sql.DB, err error) {

	switch dbc.Type {
	case strings.ToLower("mysql"):
		//always use most recent mysql changes
		db, err = sql.Open("mysql", dbc.SQLConfig.FormatDSN())
	default:
		//handle other types of connections, e.g. SQL Lite
		db, err = sql.Open(dbc.Type, dbc.Config)
	}

	if err != nil {
		return db, err
	}
	err = db.Ping()
	if err != nil {
		return db, err
	}
	return db, nil
}
