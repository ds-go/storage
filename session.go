package storage

import (
	"net/http"

	"github.com/gorilla/sessions"
)

var (
	// key must be 16, 24 or 32 bytes long (AES-128, AES-192 or AES-256)
	// key = []byte("super-secret-key")
	SessionStore *sessions.CookieStore
)

func GenerateSession(key []byte) {
	SessionStore = sessions.NewCookieStore(key)
}

func LoginExample(w http.ResponseWriter, r *http.Request) {
	session, _ := SessionStore.Get(r, "cookie-name")
	session.Values["authenticated"] = true
	session.Save(r, w)
}

func LogoutExample(w http.ResponseWriter, r *http.Request) {
	session, _ := SessionStore.Get(r, "cookie-name")
	session.Values["authenticated"] = false
	session.Save(r, w)
}
