package storage

import (
	"log"
	"os"
	"testing"

	_ "github.com/mattn/go-sqlite3"
)

var (
	dbtype   = "sqlite3"
	dbconfig = "./test.db"
	sqlTblUp = `
	CREATE TABLE TEST_TABLE(
	ID int NOT NULL,
	Test bool,
	PRIMARY KEY (ID)
	)`
	sqlTblInsert = `INSERT INTO TEST_TABLE (ID, Test) VALUES (?, ?)`
	sqlTblFetch  = `SELECT ID, Test from TEST_TABLE`
)

func NewMockDBTable() (dbc *DBC, err error) {

	dbc = NewDatabase(dbtype, dbconfig)
	db, err := dbc.Open()
	if err != nil {
		return
	}
	defer db.Close()
	stmt, err := db.Prepare(sqlTblUp)
	if err != nil {
		return
	}
	defer stmt.Close()
	res, err := stmt.Exec()
	if err != nil {
		return
	}
	_, err = res.LastInsertId()
	if err != nil {
		return
	}
	return
}

func AddMockData(dbc *DBC) error {

	db, err := dbc.Open()
	if err != nil {
		return err
	}
	defer db.Close()

	stmt, err := db.Prepare(sqlTblInsert)
	if err != nil {
		return err
	}
	defer stmt.Close()
	res, err := stmt.Exec(
		10,
		"Test",
	)
	if err != nil {
		return err
	}
	_, err = res.LastInsertId()
	if err != nil {
		return err
	}
	return err
}

//replace mysql connection with local sql lite db
func TestNewDatabase(t *testing.T) {

	defer func() {
		defer os.Remove(dbconfig)
		if r := recover(); r != nil {
			log.Fatal(r)
		}
	}()

	dbc := NewDatabase(dbtype, dbconfig)
	db, err := dbc.Open()
	if err != nil {
		panic(err.Error())
	}
	db.Close()
}

func TestDatabaseInsert(t *testing.T) {

	defer func() {
		defer os.Remove(dbconfig)
		if r := recover(); r != nil {
			log.Fatal(r)
		}
	}()

	dbc, err := NewMockDBTable()
	if err != nil {
		panic(err.Error())
	}

	err = AddMockData(dbc)
	if err != nil {
		panic(err.Error())
	}

	//test retrieve data
	db, err := dbc.Open()
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	stmt, err := db.Prepare(sqlTblFetch)
	if err != nil {
		panic(err.Error())
	}
	defer stmt.Close()
	rows, err := stmt.Query()
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	type data struct {
		ID   int
		Test string
	}

	for rows.Next() {
		res := &data{
			ID:   0,
			Test: "",
		}

		err = rows.Scan(
			&res.ID,
			&res.Test,
		)
		if err != nil {
			log.Fatal(err.Error())
		}

		if res.ID != 10 {
			log.Panicf("wrong id found, expect 10 got %v \n", res.ID)
		}
		if res.Test != "Test" {
			log.Panicf("wrong test data found, expect 10 got %v \n", res.Test)
		}
	}

}
