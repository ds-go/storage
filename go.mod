module gitlab.com/ds-go/storage

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gorilla/sessions v1.2.1
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
)
