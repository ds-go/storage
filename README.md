package storage


# How to use:

```
package main

import (
	
	gitlab.com/ds-go/storage
	_ "github.com/mattn/go-sqlite3"
)

var (
	dbtype   = "sqlite3"
	dbconfig = "./test.db"
	sqlTblUp = `
	CREATE TABLE TEST_TABLE(
	ID int NOT NULL,
	Test bool,
	PRIMARY KEY (ID)
	)`
	sqlTblInsert = `INSERT INTO TEST_TABLE (ID, Test) VALUES (?, ?)`
	sqlTblFetch  = `SELECT ID, Test from TEST_TABLE`
)


func main(){

	dbc = storage.NewDatabase(dbtype, dbconfig)
	db, err := dbc.Open()
	if err != nil {
		return
	}
	defer db.Close()
	stmt, err := db.Prepare(sqlTblUp)
	if err != nil {
		return
	}
	defer stmt.Close()
	res, err := stmt.Exec()
	if err != nil {
		return
	}
}

func AddRecord(dbc *DBC, id int) error{

	db, err := dbc.Open()
	if err != nil {
		return err
	}
	defer db.Close()

	stmt, err := db.Prepare(sqlTblInsert)
	if err != nil {
		return err
	}
	defer stmt.Close()
	
	res, err := stmt.Exec(
		10,
		"Test",
	)
	if err != nil {
		return err
	}
	_, err = res.LastInsertId()
	if err != nil {
		return err
	}
	return err
}

```
